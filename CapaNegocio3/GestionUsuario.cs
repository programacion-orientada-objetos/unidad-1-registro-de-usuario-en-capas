﻿using System;
using CapaPersistenciaDatos;
using CapaPersistenciaDatos.Modelos;

namespace CapaNegocio3

{
    public class GestionUsuario
    {

        //public Usuario Usuario { get; set; }

        public GestionUsuario()
        {
        }

        private bool ValidatePassword(string password1, string password2)
        {
            //condicional ternario
            //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/conditional-operator
            return password1.Equals(password2) ? true : false;
        }

        public bool UserRegister(string username, string password, string confirmPassword)
        {
            //condicional clásico
            bool resultado = false;
            if (ValidatePassword(password, confirmPassword) && !PersistenciaUsuario.BuscarUsuario(username))
            {
                PersistenciaUsuario.GuardarUsuario(new Usuario(username, password));
                resultado = true;
            }
            return resultado;
            
        }

        public void CambiarContraseniaUsuario(string username, string nuevaContrasenia)
        {
            //Patrón Singleton analizar el siguiente parcial
            PersistenciaUsuario.ModificarUsuario(username, nuevaContrasenia);
        }

        public string MostrarUsuarios()
        {
            return PersistenciaUsuario.RetornarUsuarios();
        }

        
    }
}
