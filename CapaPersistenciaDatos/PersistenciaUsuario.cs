﻿using System;
using System.Collections.Generic;
using CapaPersistenciaDatos.Modelos;

namespace CapaPersistenciaDatos
{
    //Entity Framework
    //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/static-classes-and-static-class-members
    public static class PersistenciaUsuario
    {
        //Lista estática que "simula" una persistencia de datos
        static List<Usuario> ListaUsuario = new List<Usuario>();

        //CRUD - 
        //Create - Guardar
        //Read - Leer 
        //Update - Modificar
        //Delete - Eliminar

        //CREATE
        public static void GuardarUsuario(Usuario usuario)
        {
            //para guardar datos en la base de datos
            //para guardar datos en el servidor REST
            ListaUsuario.Add(usuario);
        }

        //UPDATE
        //Modificar contraseña
        public static void ModificarUsuario(string username, string newPassword)
        {
            //predicados (Lambda o Linq)
            Usuario usuarioPorModificar = ListaUsuario.Find(usuario => usuario.Username == username);
            usuarioPorModificar.Password = newPassword;
        }

        //READ
        public static bool BuscarUsuario(string username)
        {

           return ListaUsuario.Exists(usuario => usuario.Username == username);
        }

        //READ
        public static string RetornarUsuarios()
        {
            string UsuariosEnTexto="";

            foreach (var item in ListaUsuario)
            {
                //String interpolation
                //UsuariosEnTexto = UsuariosEnTexto + "Usuario:" + item.Username + "y Password: " + item.Password;
                UsuariosEnTexto += $"Usuario: {item.Username} y Password: {item.Password} \n";
            }

            return UsuariosEnTexto;
        }

        //DELETE
    }
}
