﻿using System;
namespace CapaPersistenciaDatos.Modelos
{
    public class Usuario
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public Usuario(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }

        public Usuario() { }

    }
}
