﻿using System;

using CapaNegocio3;

namespace Presentacion
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al registro de usuario!");
            Console.WriteLine("Ingrese el nombre de usuario:");
            var username = "epanchana"; //Console.ReadLine();
            Console.WriteLine("Ingrese la contraseña:");
            var password = "epanchana"; //Console.ReadLine();
            Console.WriteLine("Confirme la contraseña:");
            var confirmPassword = "epanchana"; //Console.ReadLine();

            GestionUsuario gestionUsuario = new GestionUsuario();
            gestionUsuario.UserRegister(username, password, confirmPassword);
            gestionUsuario.UserRegister("epanchana", "panchana", "panchana");
            gestionUsuario.UserRegister("juan", "pueblo", "puevlo123");
            gestionUsuario.UserRegister("lalo", "landa", "landa");

            Console.WriteLine("\nLISTADO DE USUARIOS EN LA LISTA:");
            Console.WriteLine(gestionUsuario.MostrarUsuarios());

            //gestionUsuario.CambiarDatosUsuario("lalo", "nueva contrasenia");
            //Console.WriteLine(gestionUsuario.MostrarUsuarios());

        }
    }
}
